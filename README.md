# XNAT HPC Client

HPC client for the XNAT container service

# Build

```
$ make
```

# Run

```
$ cp .default.env .env # edit file for deployment
$ sudo docker run -it --rm --env-file=.env xnat_hpc_client hpcsera --help

Usage: hpcsera [OPTIONS] COMMAND [ARGS]...

  HPCSerA client for XNAT container service

Options:
  --hpc_api_host TEXT  HPC API host
  --xnat_host TEXT     XNAT host
  --xnat_alias TEXT    XNAT username or alias
  --xnat_secret TEXT   XNAT password or alias secret
  --hpc_users TEXT     HPC user store path
  --help               Show this message and exit.

```