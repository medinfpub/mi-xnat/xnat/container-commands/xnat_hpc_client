FROM python:3-bookworm
ENV DEBIAN_FRONTEND=noninteractive

ADD hpcsera /src/hpcsera
RUN pip install /src/hpcsera