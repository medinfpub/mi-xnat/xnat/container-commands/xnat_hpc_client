#!/usr/bin/env python3

import sys, os, base64, json, requests, click


def _xnat_get_user(xnat_host, xnat_alias, xnat_secret):
    resp = requests.get(
        f"{xnat_host}/data/services/tokens/validate/{xnat_alias}/{xnat_secret}",
        auth=(xnat_alias, xnat_secret)
    )
    resp.raise_for_status()
    return resp.json()["valid"]


def _hpc_post_function(func_name, func_args, hpc_api_host=None, hpc_user=None, hpc_token=None):
    payload = {"args": func_args.split(), "output":"/tmp", "input":"/tmp",
               "callback_url":"http://call-home.de"}
    resp = requests.post(
        f"{hpc_api_host}/async-function/user/{hpc_user}/project/xnat/functionname/{func_name}",
        json=payload,
        headers={"Authorization": f"Bearer {hpc_token}"}
    )
    resp.raise_for_status()
    return resp.json()


def _hpc_get_function(func_id, hpc_api_host=None, hpc_user=None, hpc_token=None):
    resp = requests.get(
        f"{hpc_api_host}/async-function/user/{hpc_user}/project/xnat/functionid/{func_id}",
        headers={"Authorization": f"Bearer {hpc_token}"}
    )
    resp.raise_for_status()
    return resp.json()


def _get_credentials(xnat_host, xnat_alias, xnat_secret, hpc_users):
    xnat_user = _xnat_get_user(xnat_host, xnat_alias, xnat_secret)
    hpc_users = json.loads(base64.b64decode(hpc_users).decode())
    return hpc_users[xnat_user]["hpc_user"], hpc_users[xnat_user]["hpc_token"]


@click.group()
@click.option("--hpc_api_host", type=str, envvar="HPC_API_HOST", help="HPC API host")
@click.option("--xnat_host", type=str, envvar="XNAT_HOST", help="XNAT host")
@click.option("--xnat_alias", type=str, envvar="XNAT_USER", help="XNAT username or alias")
@click.option("--xnat_secret", type=str, envvar="XNAT_PASS", help="XNAT password or alias secret")
@click.option("--hpc_users", type=str, envvar="HPC_USERS", help="base64 encoded HPC user store")
@click.pass_context
def cli(ctx, hpc_api_host, xnat_host, xnat_alias, xnat_secret, hpc_users):
    """HPCSerA client for XNAT container service"""
    hpc_user, hpc_token = _get_credentials(xnat_host, xnat_alias, xnat_secret, hpc_users)
    connection_params = { "hpc_api_host": hpc_api_host, "hpc_user": hpc_user, "hpc_token": hpc_token }
    ctx.obj = connection_params


@click.command()
@click.argument("func_name")
@click.argument("func_args")
@click.pass_context
def call(ctx, func_name, func_args):
    """Call function by name"""
    connection_params = ctx.obj
    resp = _hpc_post_function(func_name, func_args, **connection_params)
    click.echo(resp['functionid'])


@click.command()
@click.argument("func_id")
@click.pass_context
def status(ctx, func_id, hpc_api_host=None):
    """Get function status by id"""
    connection_params = ctx.obj
    click.echo(_hpc_get_function(func_id, **connection_params))


cli.add_command(call)
cli.add_command(status)


if __name__ == "__main__":
    cli()